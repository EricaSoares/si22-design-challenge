
# RedLight Summer Internship 2022 - Design Challenge

![banner](/assets/banner.jpg)

#

In this challenge you should **design** and **implement** a **landing page** for "O MOELAS" bar 🍷.

"O MOELAS" it's a bar with a cozy atmosphere that has been working with the academic community since 1974. It's known for its extensive list of shots and cocktails with inviting prices.

The aim of this challenge is to build a simple web page that promotes the bar. You should design an appealing layout and add some animations if you wish, in order to impress future clients 🤑.

#

### Goals

In this page we want to find:

- A short description about the bar and its history;
- Top 3 drinks;
- The bar crew;
- An image gallery (optional - only for masters 😎);
- Contacts and location.

Again, we're looking for something simple, creative an well structured.
You can use images of your choice and "Lorem ipsum" whenever you want. Colors and typography are also choosen by you.
Don't overthink 🤔.

#

### Phases

##### 1.Research 🔎
Research and show us similar solutions that you find inspirational.

##### 2.Design 🎨
Design a high-fidelity mockup for your proposal. You can use any tool you want such as Figma, Adobe XD, Sketch, etc.

##### 3.Build 💻
Implement the designed page using HTML and CSS (You can also use SCSS, SASS, LESS or other type of styling).
If you're a legend, you can also add some JavaScript for animations or any dynamic content you want to do.

#

### Delivery
When you're done, you should fork this repository and upload your work there to share it with us or you can simply send everything in a .zip folder or a WeTransfer link.
Please try to share your process going through the steps you took to reach your final version.
